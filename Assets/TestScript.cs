﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Abela Paolo
 * 
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;

///<summary>
///
///</summary>

public class TestScript : MonoBehaviour
{
    public GameObject[] oggetti;

    public int publica = 10; //sempre visibile nell'inspector
    private int privata = 5; //visibile nell'inspector in debug mode
    protected int protetta = 1; //visibile nell'inspector in debug mode

    [ContextMenu("Funzione di saluto")]
	void Saluta()
    {
        Debug.Log("CIao Mondo!");
    }
}
